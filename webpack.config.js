const path 					= require('path');
const webpack 				= require('webpack');
const autoprefixer 			= require('autoprefixer');
const UglifyJsPlugin 		= webpack.optimize.UglifyJsPlugin;
const env 					= process.env.WEBPACK_ENV;
const CleanWebpackPlugin 	= require('clean-webpack-plugin');
const HtmlWebpackPlugin 	= require('html-webpack-plugin');
const ExtractTextPlugin 	= require('extract-text-webpack-plugin');
require("babel-polyfill");

var LodashModuleReplacementPlugin = require('lodash-webpack-plugin');

const PATHS = {
	app: path.resolve(__dirname, './src'),
	demo: path.resolve(__dirname, './demo'),
	styles: path.resolve(__dirname, './src/styles'),
	images: path.resolve(__dirname, './src/images'),
	fonts: path.resolve(__dirname, './src/fonts'),
	sounds: path.resolve(__dirname, './src/sounds'),
	build: path.resolve(__dirname, './build')
};

var plugins = [
	new CleanWebpackPlugin(['build'], {
			verbose: true
		}),
	new ExtractTextPlugin('css/app.css', { allChunks: true })
];

var outputFile, entry,dest;

if (env === 'build') {
	plugins.push(
		new LodashModuleReplacementPlugin,
		new webpack.optimize.OccurrenceOrderPlugin,
		new webpack.DefinePlugin({
			'process.env': {
				// This has effect on the react lib size
				'NODE_ENV': JSON.stringify('production'),
			}
		}),
		new UglifyJsPlugin({
			minimize: true ,
			drop_console:true
		}));
	outputFile =  'rssearch.min.js';
	entry = __dirname + '/src/index.js';
	dest = PATHS.build;

} else {
	outputFile = 'demo.js';
	entry = ['babel-polyfill',__dirname + '/demo/demo.js'],
	dest = path.resolve(__dirname, 'devdemo');
	plugins.push(new HtmlWebpackPlugin({
		title: 'Custom template',
		template: __dirname + '/demo/index.html',
	}))
}

const sassLoaders = [
	'css-loader',
	'postcss-loader',
	'sass-loader?outputStyle=compressed'
];


var config = {
	entry: entry,
	devtool: 'source-map',
	output: {
		path: dest,
		filename: outputFile,
		library: 'RSSearch',
		libraryTarget: 'umd',
		umdNamedDefine: true
	},
	module: {
		loaders: [

			{
				test: /\.html$/,
				loader: 'html'
			},
			{
				test: /\.scss$/,
				loader: ExtractTextPlugin.extract('style-loader', sassLoaders.join('!'))
			},
			{
				test: /\.css$/,
				include: [
					PATHS.styles,
					PATHS.demo,
				],
				loader: ExtractTextPlugin.extract('style-loader', 'css-loader!postcss-loader')
			},
			{
				test: /\.js$/,
				loader: 'babel-loader',
				include: [
					PATHS.app,
					PATHS.demo
				],
				//include: [
				//	path.resolve(__dirname, 'src')
				//],
				query: {
					presets: ['es2015'],
					'plugins': ['lodash'],
				}
			}
		]
	},
	stats: {
		colors: true
	},
	resolve: {
		extensions: ['', '.js', '.css', '.scss']
	},
	plugins: plugins,
	postcss: function () {
		return [autoprefixer({
			browsers: ['last 2 versions']
		})];
	}
};

module.exports = config;
