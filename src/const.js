

const OPERANDS = [':', '<', '>' ];

const EVENTS = {
	'SEARCH': 'search',
	'SELECT':'select'
}

export {OPERANDS, EVENTS };