import {EVENTS, OPERATORS} from './const'


export default class Popdrop {
	constructor(dom, options) {
		this.dom = dom;
		this.options = options
		this.wrapper = dom.getElementsByTagName('rs-wrapper')[0];
		this.item = this.wrapper.getElementsByTagName('rs-item')[0];
		this.input = this.dom.getElementsByTagName('input')[0];
		this.selected = this.item.getElementsByTagName('rs-item-selected')[0];
		this.bindedHide = this.hide.bind(this);

	}

	clearWrapper(){
		while (this.wrapper.firstChild) {
			this.wrapper.removeChild(this.wrapper.firstChild);
		}
	}
	addItem(text, hl){
		const rgx = new RegExp('('+hl+')', 'gi');
		const newItem = this.item.cloneNode(true);


		const textByHl = text.split(rgx)

		textByHl.forEach((item) => {
			if(item.toLowerCase() === hl.toLowerCase()){
				const selectItem = this.selected.cloneNode(true);
				selectItem.innerHTML = item;
				newItem.appendChild(selectItem);
			}else{
				var node = document.createTextNode(item);
				newItem.appendChild(node);
			}
		});

		newItem.addEventListener('click',this.onItemSelected.bind(this,{text:text}))

		this.wrapper.appendChild(newItem);
	}

	onItemSelected(item){
		const onSelectEvent = new CustomEvent(this.options.eventPrefix+":"+EVENTS.SELECT, {
			detail: {value: item}
		});

		this.dom.dispatchEvent(onSelectEvent);
		this.hide()
	}

	hide(e){
		//if click was on current input - ignore click
		if(e && e.target && e.target == this.input){
			return;
		}
		this.wrapper.className = "";
		this.globalClickUnsubscribe();
	}

	show(){
		this.globalClickUnsubscribe();
		this.globalClickSubscribe();
		this.wrapper.className = "shown";
	}

	globalClickSubscribe(){
		window.addEventListener('click', this.bindedHide);
	}

	globalClickUnsubscribe(){
		window.removeEventListener('click', this.bindedHide)
	}

	render(data, highlight){
		this.clearWrapper();
		if(data.length>0){
			data.forEach(item => this.addItem(item,highlight))
			this.show();
		}else{
			this.hide();
		}


	}

}