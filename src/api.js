




export default class API {
	constructor(API_KEY, index, options) {
		this.baseURL = 'https://api.repustate.com/v4/'+API_KEY;
		//this.baseURL = 'http://54.90.193.5/v4/'+API_KEY;
		this.counter = 0;
		this.index = index;
		this.options = options;
		this.deepSearch = this.deepSearch.bind(this)
	}


	getMetas(query){
		return this.send('/search-metadata.json',{classification:query})
			.then(function(res){
				try{
					const data = JSON.parse(res.metadata)
					return data;
				}catch(err){
					if(this.options.debug){
						console.error(err);
					}
					return Promise.reject(err)
				}
			})
	}

	getClassifications(){
		return this.send('/search-classifications.json',{index: this.index})
			.then((res) => {
				try{
					const data = JSON.parse(res.classifications)
					return data;
				}catch(err){
					if(this.options.debug) {
						console.error(err);
					}
					return Promise.reject(err)
				}
			})
	}

	deepSearch(query){
		return this.send('/search.json',{index: this.index, query:query})// cursor:0,
			.then((res) => {
				try{
					if(!res || !res.matches){
						return Promise.reject(err)
					}
					if(typeof res.matches == 'string'){
						const data = JSON.parse(res.matches);
						return data;
					}else{
						return res.matches
					}

				}catch(err){
					if(this.options.debug) {
						console.error(err);
					}
					return Promise.reject(err)
				}
			})
	}

	send(url,data){
		return new Promise((resolve,reject) => {
			const cbname = 'processJSONPResponse_'+ (this.counter++ % 999999);
			let fullUrl = this.baseURL+url+'?callback='+cbname;
			if(data){
				Object.keys(data).forEach((dataKey) => {
					fullUrl += '&'+dataKey+'='+	data[dataKey];
				})
			}
			window[cbname] = (json) => {
				if(this.options.debug) {
					console.log('[ RS.API < ] ', url, json);
				}
				return resolve(json);
			};
			if(this.options.debug){
				console.log('[ RS.API > ] ',url);
			}
			var script = document.createElement('script');
			script.type = 'text/javascript';
			script.async = true;
			script.src = fullUrl;
			document.querySelector('head').appendChild(script);
			//resolve({data:['opop.123','opop.234']});
		})



	}

	//httpGet(aUrl, aCallback) {
	//	var anHttpRequest = new XMLHttpRequest();
	//	anHttpRequest.onreadystatechange = function() {
	//		if (anHttpRequest.readyState == 4 && anHttpRequest.status == 200){
	//			return aCallback(null, anHttpRequest.responseText);
	//		}else{
	//			return aCallback(anHttpRequest);
	//		}
	//
	//	}
	//
	//	anHttpRequest.open( "GET", aUrl, true );
	//	return anHttpRequest.send( null );
	//}
}