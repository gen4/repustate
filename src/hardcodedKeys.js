export default  {
  "sentiment":  [
      "neutral",
      "negative",
      "positive"
  ],

  "theme":[
      "hardcoded1",
      "hardcoded2"
   ],

  "lang": [
      "ru-RU",
      "en-US"
  ]

}