import {EVENTS, OPERATORS} from './const'


import Searcher from './searcher'
import Popdrop from './popdrop'
import QueryParser from './queryparser'


const DEFAULT_OPTIONS = {
	'placeholder':'Enter a text',
	'eventPrefix':'rssearch',
	'searchFullLine':false,
	'searchMinChars':3,
	'debug':false
}

function CustomEvent (event, params) {
	params = params || { bubbles: false, cancelable: false, detail: undefined };
	var evt = document.createEvent('CustomEvent');
	evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
	return evt;
};
CustomEvent.prototype = window.CustomEvent.prototype;
window.CustomEvent = CustomEvent;


class RSSearch {
	constructor(searchbarDom, API_KEY, index, options) {
		//TODO Check for required params


		this.searchbarDom = searchbarDom;
		this.index = index;

		this.options = {};
		Object.keys(DEFAULT_OPTIONS).forEach((key) => {
			this.options[key] = options[key] || DEFAULT_OPTIONS[key];
		});
		this.checkStructure()

		this.searcher = new Searcher(API_KEY, searchbarDom, index, this.options);
		this.popdrop = new Popdrop(searchbarDom,this.options);
		this.button.addEventListener('click',() => {
			this.doDeepSearch()
		});
		this.initEvents();
	}


	doDeepSearch(){

		this.searcher.deepSearch(this.input.value)
			.then((results) => {
				const onSelectEvent = new CustomEvent(this.options.eventPrefix+":"+EVENTS.SEARCH, {
					detail: {value: results}
				});

				document.createEvent("CustomEvent");
				this.searchbarDom.dispatchEvent(onSelectEvent);
				return results
			})
	}

	checkStructure(){
		this.input = this.searchbarDom.getElementsByTagName('input')[0];
		if(!this.input){
			this.input = document.createElement('input');
			this.input.type = "text";
			this.input.placeholder = this.options.placeholder;
			this.searchbarDom.appendChild(this.input);
		}

		this.button = this.searchbarDom.getElementsByTagName('button')[0];
		if(!this.button){
			this.button = document.createElement('button');
			this.button.className = "clickable";
			this.searchbarDom.appendChild(this.button);
		}

		this.wrapper = this.searchbarDom.getElementsByTagName('rs-wrapper')[0];
		if(!this.wrapper){
			this.wrapper = document.createElement('rs-wrapper');
			this.searchbarDom.appendChild(this.wrapper);
		}

		this.item = this.wrapper.getElementsByTagName('rs-item')[0];
		if(!this.item){
			this.item = document.createElement('rs-item');
			this.item.className = "clickable";
			this.wrapper.appendChild(this.item);
		}

		this.itemSelected = this.item.getElementsByTagName('rs-item-selected')[0];
		if(!this.itemSelected){
			this.itemSelected = document.createElement('rs-item-selected');
			this.item.appendChild(this.itemSelected);
		}
	}


	initEvents(){

		function cursorChange(e){
			//parse value
			console.log('initEvents',e)
			if(e && e.keyCode && e.keyCode == 13){
				this.doDeepSearch();
				return;
			}
			setTimeout(() => {
				let qp = new QueryParser(this.input.value);
				let term = qp.getTextByPos(this.input.selectionStart);
				this.searcher.search(term)
					.then(items => {
						this.popdrop.render(items,term)
					})
			},1);
		}

		let cursorChangeBinded = cursorChange.bind(this);

		this.searchbarDom.addEventListener(this.options.eventPrefix+':select', (e) =>{
			let qp = new QueryParser(this.input.value);
			qp.replaceTerm(e.detail.value.text, this.input.selectionStart);
			this.input.value = qp.getFullText();
			this.input.focus()
		});

		this.input.addEventListener('focus', () =>
		{
			this.input.addEventListener('keydown',cursorChangeBinded, true);
			this.input.addEventListener('click',cursorChangeBinded);
			cursorChangeBinded()
		});

		this.input.addEventListener('blur', () => {
			this.input.removeEventListener('keydown',cursorChangeBinded, true)
			this.input.removeEventListener('click',cursorChangeBinded);
		});




	}

	onResults(cb){

	}

	addEventListener(eventId, cb){
		this.searchbarDom.addEventListener(this.options.eventPrefix+':'+eventId, cb)
	}

	removeEventListener(eventId, cb){
		this.searchbarDom.removeEventListener(this.options.eventPrefix+':'+eventId, cb)
	}
}

export default RSSearch;
module.exports = RSSearch;