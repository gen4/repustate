

export default class QueryParser{
	constructor(query) {
		this.query = query;
		this.parsedQuery = this.parseQuery(query);
	}

	parseQuery(query){
		const rgx = new RegExp('\\s', 'gi');
		const parts = query.split(rgx);
		let res = [];
		let cursor = 0;
		parts.forEach((part) => {
			res.push({
				string:part,
				start:cursor,
				end:cursor+part.length
			});

			cursor+=part.length+1
		});

		return res;
	}

	replaceTerm(newTerm, pos){
		let newParts = JSON.parse(JSON.stringify(this.parsedQuery))

		let d = 0;
		newParts.forEach((part) => {
			if(pos>=part.start && pos<=part.end){
				part.string = newTerm;
				d =  newTerm.length -  (part.end - part.start);
				part.end = part.start +  newTerm.length ;
			}else{
				part.start += d;
				part.end += d;
			}
		});
		this.parsedQuery = newParts;
		return newParts;
	}

	getTextByPos(pos){
		for(let index in this.parsedQuery){
			let part = this.parsedQuery[index];
			if(pos>=part.start && pos<=part.end){
				return part.string;
			}
		}
		return null;
	}

	getText(parts){
		var text = parts.reduce((total, part) => {
			return total + part.string+' '
		},'')
		text = text.substr(0,text.length - 1)
		return text;
	}

	getFullText(){
		return this.getText(this.parsedQuery)
	}
}