export default  {
  "sentiment": {
    "values": [
      "neutral",
      "negative",
      "positive"
    ]
  },
  "theme":{
    "values": [
      "hardcoded1",
      "hardcoded2"
    ]
  },
  "lang":{
    "values": [
      "ru-RU",
      "en-US"
    ]
  }
}