const splitSymbol = ':';
import API from './api';
import hardcodedValues from "./hardcodedKeys"

export default class Searcher {


	constructor(API_KEY, searchbarDom, index, options) {
		this.dom = searchbarDom;
		this.options = options;
		this.API = new API(API_KEY, index, options);
		this.API.getClassifications()
			.then((cls)=>{
				this.dataSplited['classifications'] = cls;
				this.dataFlat = this.dataFlat.concat(cls)
			});
		this.originalHardcodedData 	= hardcodedValues;
		this.dataSplited 	= this.parseDataSplited(this.originalHardcodedData);
		this.dataFlat 		= this.parseDataFlat(this.originalHardcodedData);
	}

	parseDataSplited(originalData){
		return originalData;
	}

	parseDataFlat(originalData){
		return Object.keys(originalData).reduce((total,key) => {
			Array.prototype.push.apply(total,originalData[key].map(item => key + splitSymbol + item))
			return total;
		},[])
	}

	deepSearch(query){
		return this.API.deepSearch(query)
	}

	search(query){
		return new Promise((resolve, reject) => {

			console.log('query.length<this.options.searchMinChars',query.length,'<',this.options.searchMinChars)
			if(!query || query == '' || query.length<this.options.searchMinChars){
				return resolve([]);
			}

			let rgx;
			if(this.options.searchFullLine){
				rgx = new RegExp(query, 'i');
			}else{
				rgx = new RegExp('^'+query, 'i');
			}

			const flatResults = this.dataFlat.filter((item) => {
				return rgx.test(item) && item.toLowerCase()!=query.toLowerCase()
			});
			const queryByDots = query.split('.')
			const classificationName = queryByDots[0]+'.'+queryByDots[1]
			const shouldSearchMetas = this.dataSplited.classifications.indexOf(classificationName)>=0;

			if(shouldSearchMetas){
				if(this.previousClassification != classificationName){
					this.previousClassification = classificationName
					return this.API.getMetas(query)
						.then((metas)=>{
							let upd = metas.map(item => {
								return query+'.'+item
							});
							this.previousClassificationData = upd;
							return resolve(flatResults.concat(upd));
						});
				}else{
					const metaResults = this.previousClassificationData.filter((item) => {
						return rgx.test(item) && item.toLowerCase()!=query.toLowerCase()
					});
					return resolve(flatResults.concat(metaResults));
				}

			}else{
				return resolve(flatResults);
			}
		})
	}

}