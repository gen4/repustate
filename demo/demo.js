import RSSearch from '../src/index';
import scss from './style';

document.addEventListener("DOMContentLoaded", function(){

	const seachbarDOM = document.getElementById("seachbar");
	const API_KEY = 'e78ed1ac8c02262023f2ba062202fb39efe3412d';
	const index = 'test-index';
	const options = {
		placeholder:'Start typing...',
		eventPrefix:'rssearchcustomprefix',
		debug:true
	}
	const searchbar = new RSSearch(seachbarDOM, API_KEY, index, options);

	searchbar.addEventListener('select', (e) =>{
		//item selected
	});
	searchbar.addEventListener('search', (e) =>{
		//seach
		clearContainer();
		fillContainerWithResults(e.detail.value)
	});

	const searchResultsContainer = document.getElementById("search-results");

	const clearContainer = function(){
		while (searchResultsContainer.firstChild) {
			searchResultsContainer.removeChild(searchResultsContainer.firstChild);
		}
	}

	const fillContainerWithResults = function(results){
		results.forEach((item) => {
			let div  = document.createElement('div');
			div.className="result-item"
			div.innerText = JSON.stringify(item);
			searchResultsContainer.appendChild(div);
		})

	}
});



